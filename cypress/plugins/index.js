/* eslint-disable */
/// <reference types="cypress" />
// ***********************************************************
// This example plugins/index.js can be used to load plugins
//
// You can change the location of this file or turn off loading
// the plugins file with the 'pluginsFile' configuration option.
//
// You can read more here:
// https://on.cypress.io/plugins-guide
// ***********************************************************

// This function is called when a project is opened or re-opened (e.g. due to
// the project's config changing)

const { set, get } = require('lodash');
const path = require('path');
const fs = require('fs').promises;
const pathToFile = '../../tmp/shop_configuration.json';
const dir = '../../tmp';

async function fileExist(path) {
    try {
        await fs.access(path);
        return true;
    } catch {}

    return false;
}

module.exports = (on, config) => {
    on('task', {
        async setConfigKey({ key, value }) {
            let shopsIds = {};

            try {
                const fileContent = await fs.readFile(path.join(__dirname, pathToFile));
                shopsIds = JSON.parse(fileContent.toString());
            } catch (e) {
                if (!(await fileExist(path.join(__dirname, dir)))) {
                    await fs.mkdir(path.join(__dirname, dir));
                }

                console.warn('Read file error occurred');
            }

            const data = set(shopsIds, key, value);

            await fs.writeFile(path.join(__dirname, pathToFile), JSON.stringify(data, null, 2), { encoding: 'utf-8' });
            return true;
        },
        async getConfigKey(key) {
            const shopsIds = await fs.readFile(path.join(__dirname, pathToFile));
            return get(shopsIds ? JSON.parse(shopsIds.toString()) : {}, key);
        },
    });
};
