/* eslint-disable */
// ***********************************************************
// This example support/index.js is processed and
// loaded automatically before your test files.
//
// This is a great place to put global configuration and
// behavior that modifies Cypress.
//
// You can change the location of this file or turn off
// automatically serving support files with the
// 'supportFile' configuration option.
//
// You can read more here:
// https://on.cypress.io/configuration
// ***********************************************************

// Alternatively you can use CommonJS syntax:
// require('./commands')
require('./command/api/customer');
require('./command/api/systemSetting');
require('./command/api/category');
require('./command/api/tariff');
require('./command/api/warehouse');
require('./command/api/product');
require('./command/api/visibility');
require('./command/api/cron');
require('./command/api/transportMethod');
require('./command/api/paymentMethod');
require('./command/api/weightRangeAdFinalizationOption');
require('./command/api/finalizationOption');
require('./command/api/module');
require('./command/front/customer');
require('./command/front/default');
require('./command/front/product');
require('./command/front/basket');
require('./command/admin/login');
