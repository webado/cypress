const { login, password } = Cypress.config('basic_auth');

export const basicAuth = {
    user: login,
    pass: password,
};
