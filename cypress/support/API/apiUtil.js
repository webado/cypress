import { basicAuth } from './authorization/basicAuth.js';

export const request = (url, method = 'GET', body = null, auth = basicAuth) => {
    return cy.request({
        url,
        method,
        auth,
        body,
    });
};
