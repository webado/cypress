import { request } from '../apiUtil.js';
const baseUrl = Cypress.config('baseUrl');
const uri = Cypress.config('api').shop_api;

export const get = (endpoint) => {
    return request(baseUrl + uri + endpoint);
};

export const post = (endpoint, body) => {
    return request(baseUrl + uri + endpoint, 'POST', body);
};

export const patch = (endpoint, body) => {
    return request(baseUrl + uri + endpoint, 'PATCH', body);
};

export const remove = (endpoint, body) => {
    return request(baseUrl + uri + endpoint, 'DELETE', body);
};
