function setConfigValue(key, value) {
    cy.task('setConfigKey', { key, value });
}

function getConfigValue(key) {
    return cy.task('getConfigKey', key);
}

export { setConfigValue, getConfigValue };
