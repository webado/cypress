const { login, password } = Cypress.config('basic_auth');

Cypress.Commands.add('loginToAdminPanel', () => {
    cy.get('#field-login').clear().type(login);
    cy.get('#field-password').clear().type(password);
    cy.get('[data-qa_do_login_button]').click();
});
