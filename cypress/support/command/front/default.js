Cypress.Commands.add('checkErrorVisibility', (element) => {
    cy.get(element).should('be.visible');
});

Cypress.Commands.add('clickRegisterButton', () => {
    cy.get('[data-qa-register=""]').click();
});

Cypress.Commands.add('clickLoginButton', () => {
    cy.get('[data-qa-login_button]').click();
});
