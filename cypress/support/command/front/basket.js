Cypress.Commands.add(
    'chooseTransportMethod',
    (transportMethodId, paymentMethodId) => {
        cy.get(
            '#finalizationOption-' + transportMethodId + '-' + paymentMethodId
        ).check();
    }
);

Cypress.Commands.add('clickOrderButton', () => {
    cy.get('[data-qa-send_order]').click();
});
