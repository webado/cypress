Cypress.Commands.add('customerLogin', (email, password) => {
    cy.get('input#field_7').clear().type(email);
    cy.get('input#field_8').clear().type(password);
});

Cypress.Commands.add('loginCustomerAndClickButton', (email, password) => {
    cy.get('input#field_7').clear().type(email);
    cy.get('input#field_8').clear().type(password);
    cy.clickLoginButton();
});

Cypress.Commands.add('fillCustomerData', (customer) => {
    cy.get('[data-qa-first_name] input').clear().type(customer.firstName);
    cy.get('[data-qa-last_name] input').clear().type(customer.lastName);
    cy.get('[data-qa-telephone] input').clear().type(customer.telephone);
    cy.get('[data-qa-email] input').clear().type(customer.email);
    cy.get('[data-qa-full_name] input')
        .clear()
        .type(customer.firstName + ' ' + customer.lastName);
    cy.get('[data-qa-street_name] input').clear().type(customer.streetName);
    cy.get('[data-qa-building_number] input')
        .clear()
        .type(customer.buildingNumber);
    cy.get('[data-qa-city] input').clear().type(customer.city);
    cy.get('[data-qa-postal_code] input').clear().type(customer.postalCode);
});

Cypress.Commands.add(
    'fillCustomerPassword',
    (password, confirmationPassword) => {
        cy.get('[data-qa-password] input').clear().type(password);
        cy.get('[data-qa-password_confirmation] input')
            .clear()
            .type(confirmationPassword);
    }
);

Cypress.Commands.add('clearAllCustomerRegistretionData', () => {
    cy.get('[data-qa-first_name] input').clear();
    cy.get('[data-qa-last_name] input').clear();
    cy.get('[data-qa-telephone] input').clear();
    cy.get('[data-qa-email] input').clear();
    cy.get('[data-qa-password] input').clear();
    cy.get('[data-qa-password_confirmation] input').clear();
    cy.get('[data-qa-full_name] input').clear();
    cy.get('[data-qa-street_name] input').clear();
    cy.get('[data-qa-building_number] input').clear();
    cy.get('[data-qa-city] input').clear();
    cy.get('[data-qa-postal_code] input').clear();
});

Cypress.Commands.add('checkAllCustomerRegistrationInputErrors', () => {
    cy.checkErrorVisibility('[data-qa-first_name] span');
    cy.checkErrorVisibility('[data-qa-last_name] span');
    cy.checkErrorVisibility('[data-qa-telephone] span');
    cy.checkErrorVisibility('[data-qa-email] span');
    cy.checkErrorVisibility('[data-qa-password] span');
    cy.checkErrorVisibility('[data-qa-full_name] span');
    cy.checkErrorVisibility('[data-qa-street_name] span');
    cy.checkErrorVisibility('[data-qa-building_number] span');
    cy.checkErrorVisibility('[data-qa-city] span');
    cy.checkErrorVisibility('[data-qa-postal_code] span');
});
