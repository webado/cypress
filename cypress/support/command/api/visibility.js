import * as api from '../../API/service/shopApiHelper.js';

Cypress.Commands.add('visibileProductInAlias', (productIds) => {
    api.post('product/visibility/enable', productIds);
});
