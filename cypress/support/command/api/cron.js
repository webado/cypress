import * as api from '../../API/service/shopApiHelper.js';

Cypress.Commands.add('runCron', (name) => {
    api.get('cron_task/cron_tasks_run/' + name);
});
