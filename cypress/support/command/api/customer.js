import * as api from '../../API/service/shopApiHelper.js';

Cypress.Commands.add('createCustomer', (customer) => {
    api.post('customer', customer);
});
