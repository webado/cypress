import * as api from '../../API/service/shopApiHelper.js';

Cypress.Commands.add('createPaymentMethod', (paymentMethod) => {
    api.post('payment_method', paymentMethod);
});
