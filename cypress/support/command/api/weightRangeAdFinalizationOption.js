import * as api from '../../API/service/shopApiHelper.js';

Cypress.Commands.add(
    'createWeightRangeAndFinalizationOption',
    (transportMethodId, weightRangeAndPaymentMethod) => {
        api.post(
            'transport_method/' + transportMethodId + '/weight',
            weightRangeAndPaymentMethod
        );
    }
);
