import * as api from '../../API/service/shopApiHelper.js';

Cypress.Commands.add('createCategory', (category) => {
    api.post('category', category);
});
