import * as api from '../../API/service/shopApiHelper.js';

Cypress.Commands.add('createPriceList', (priceList) => {
    api.post('tariff/price_list', priceList);
});

Cypress.Commands.add('createStrategy', (strategy) => {
    api.post('tariff/strategy', strategy);
});

Cypress.Commands.add('moveStrategyToTop', (data) => {
    api.post('tariff/strategy/top', data);
});

Cypress.Commands.add('updateTariffPriceInProduct', (tariffPrice) => {
    api.post('tariff/price', tariffPrice);
});
