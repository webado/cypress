import * as api from '../../API/service/shopApiHelper.js';

Cypress.Commands.add('createTransportMethod', (transportMethod) => {
    api.post('transport_method', transportMethod);
});
