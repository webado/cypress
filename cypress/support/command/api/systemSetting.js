import * as api from '../../API/service/shopApiHelper.js';

Cypress.Commands.add('findSystemSetting', (params) => {
    api.get('system_setting' + params);
});

Cypress.Commands.add('updateSystemSetting', (id, data) => {
    api.patch('system_setting/' + id, {
        data,
    });
});
