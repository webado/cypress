import * as api from '../../API/service/shopApiHelper.js';

Cypress.Commands.add('installModule', (moduleName) => {
    api.post('module/' + moduleName, {});
});
