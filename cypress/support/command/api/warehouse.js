import * as api from '../../API/service/shopApiHelper.js';

Cypress.Commands.add('createWarehouse', (warehouse) => {
    api.post('warehouse', warehouse);
});

Cypress.Commands.add('reciveVariantInWarehouse', (warehouseId, data) => {
    api.patch('warehouse/' + warehouseId + '/receive', data);
});
