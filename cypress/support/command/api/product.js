import * as api from '../../API/service/shopApiHelper.js';

Cypress.Commands.add('createProduct', (product) => {
    api.post('product', product);
});

Cypress.Commands.add('assignCategoryToProduct', (categoryId, data) => {
    api.post('category/' + categoryId + '/product', data);
});
