import customer from '../fixtures/customer.json';
import { randomString } from '../support/service/randomGeneratorHelper.js';

const countryId = Cypress.config('default').countryId;

describe('Login to a customer account', () => {
    const email = randomString(8) + '_cypress@test.pl';
    const password = '111222';
    const firstName = 'Lucjan';
    const lastName = 'Waligóra';

    before(() => {
        cy.createCustomer({
            ...customer,
            email,
            firstName,
            lastName,
            password,
            delivery: {
                ...customer.delivery,
                countryId,
            },
        });

        cy.visit('/customer/login');
    });

    it('Login customer with empty data', () => {
        cy.clickLoginButton();
        cy.checkErrorVisibility('.g--error');
    });

    it('Login customer with not exsiting email', () => {
        cy.customerLogin('test1123123123@test.pl', password);
        cy.clickLoginButton();
        cy.checkErrorVisibility('.g--error');
    });

    it('Login customer with bad password', () => {
        cy.customerLogin(email, '&73647');
        cy.clickLoginButton();
        cy.checkErrorVisibility('.g--error');
    });

    it('Login customer', () => {
        cy.customerLogin(email, password);
        cy.clickLoginButton();
    });

    it('confirm customer data', () => {
        cy.get('.user__info > .basic__info > .name').should('contain', firstName + ' ' + lastName);
        cy.get('[data-qa_customer_email]').should('contain', email);
    });
});
