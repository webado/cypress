import { randomString } from '../support/service/randomGeneratorHelper.js';

describe('Customer registration', () => {
    const customerRegistrationPage = '/customer/register';
    const customer = {
        firstName: 'testowy',
        lastName: 'tescik',
        telephone: '123456789',
        email: randomString(8) + '_cypress@test.pl',
        streetName: 'testowicka',
        buildingNumber: '12',
        city: 'Testowice',
        postalCode: '44-100',
    };

    const password = '444222';
    const confirmationPassword = '444222';

    before(() => {
        cy.visit(customerRegistrationPage);
    });

    it('Register customer with bad email', () => {
        cy.fillCustomerData({ ...customer, email: 'test' });
        cy.fillCustomerPassword(password, confirmationPassword);
        cy.clickRegisterButton();
        cy.checkErrorVisibility('[data-qa-email] span');
    });

    it('Register customer without first name', () => {
        cy.fillCustomerData(customer);
        cy.fillCustomerPassword(password, confirmationPassword);
        cy.get('[data-qa-first_name] input').clear();
        cy.clickRegisterButton();
        cy.checkErrorVisibility('[data-qa-first_name] span');
    });

    it('Register customer without last name', () => {
        cy.fillCustomerData(customer);
        cy.fillCustomerPassword(password, confirmationPassword);
        cy.get('[data-qa-last_name] input').clear();
        cy.clickRegisterButton();
        cy.checkErrorVisibility('[data-qa-last_name] span');
    });

    it('Register customer without any data', () => {
        cy.clearAllCustomerRegistretionData();
        cy.clickRegisterButton();
        cy.checkAllCustomerRegistrationInputErrors();
    });

    it('Register customer', () => {
        cy.fillCustomerData(customer);
        cy.fillCustomerPassword(password, confirmationPassword);
        cy.clickRegisterButton();
    });
});
