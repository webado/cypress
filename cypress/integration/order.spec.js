/// <reference types="cypress" />

import { randomString } from '../support/service/randomGeneratorHelper.js';
import { getConfigValue } from '../support/service/shopConfigurationManager';
import customer from '../fixtures/customer.json';

const countryId = Cypress.config('default').countryId;

describe('Purchasing process', () => {
    const email = randomString(8) + '_cypress@test.pl';
    const password = '111222';
    let transportMethodId;
    let paymentMethodId;
    let productId;

    before(() => {
        getConfigValue('transport_method.default').then((transport) => {
            transportMethodId = transport;
        });
        getConfigValue('payment_method.default').then((payment) => {
            paymentMethodId = payment;
        });
        getConfigValue('product.default.id').then((product) => {
            productId = product;
        });

        cy.createCustomer({
            ...customer,
            email,
            password,
            delivery: {
                ...customer.delivery,
                countryId,
            },
        });
    });

    const customerData = {
        firstName: 'testowy',
        lastName: 'tescik',
        telephone: '123456789',
        email: randomString(8) + '_cypress@test.pl',
        password: '111222',
        streetName: 'testowicka',
        buildingNumber: '12',
        city: 'Testowice',
        postalCode: '44-100',
    };

    it('Create order by one time customer', () => {
        cy.visit('products/' + productId);
        cy.clickAddProductToCartButton();
        cy.visit('/cart');
        cy.chooseTransportMethod(transportMethodId, paymentMethodId);
        cy.fillCustomerData(customerData);
        cy.clickOrderButton();
        cy.get('.cart__completed > .cart__completed__header').should(
            'contain',
            'Zamówienie zostało złożone pomyślnie!',
        );
    });

    it('Create order by regular customer', () => {
        cy.visit('/customer/login');
        cy.loginCustomerAndClickButton(email, password);
        cy.visit('products/' + productId);
        cy.clickAddProductToCartButton();
        cy.visit('/cart');
        cy.chooseTransportMethod(transportMethodId, paymentMethodId);
        cy.clickOrderButton();
        cy.get('.cart__completed > .cart__completed__header').should(
            'contain',
            'Zamówienie zostało złożone pomyślnie!',
        );
    });
});
