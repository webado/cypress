import { setConfigValue } from '../../support/service/shopConfigurationManager';
import priceList from "../../fixtures/tariff_price_list.json";
import tariffStrategy from "../../fixtures/tariff_strategy.json";

describe('Prepare tariff', () => {
    it('Create price list, strategy and move it to top', () => {
        cy.createPriceList(priceList).then((tariff) => {
            const tariffId = tariff.body.id;
            setConfigValue('tariff.list.default', tariffId);
            cy.createStrategy({
                ...tariffStrategy,
                tariffId: {
                    buy: tariffId,
                    list: tariffId,
                    base: { tariffId },
                    sell: { tariffId },
                },
            }).then((strategy) => {
                setConfigValue('tariff.strategy.default', strategy.body.id);
                cy.moveStrategyToTop({
                    strategyId: strategy.body.id,
                });
            });
        });
    });
});
