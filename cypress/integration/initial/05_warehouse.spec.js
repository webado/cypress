import { setConfigValue } from '../../support/service/shopConfigurationManager';
import warehouse from "../../fixtures/warehouse.json";
import {randomString} from "../../support/service/randomGeneratorHelper";

describe('Prepare warehouse', () => {
    it('Create warehouse', () => {
        const aliasId = Cypress.config('default').aliasId;
        cy.createWarehouse({
            ...warehouse,
            warehouse: {
                ...warehouse.warehouse,
                symbol: randomString(4) + '_' + randomString(2),
                sourceId: randomString(4) + '_' + randomString(2),
            },
            alias: {
                [aliasId]: true,
            },
        }).then((warehouse) => {
            setConfigValue('warehouse.default', warehouse.body.id)
        });
    });
});
