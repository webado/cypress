import { setConfigValue } from '../../support/service/shopConfigurationManager';
import paymentMethod from "../../fixtures/payment_method.json";

describe('Prepare payment method', ()=> {
    it('Create payment method', () => {
        cy.createPaymentMethod(paymentMethod).then((paymentMethod) => {
            setConfigValue('payment_method.default', paymentMethod.body.id)
        });
    });
})