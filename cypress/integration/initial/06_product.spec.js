import { setConfigValue, getConfigValue } from '../../support/service/shopConfigurationManager';
import product from "../../fixtures/product.json";
import tariffPrice from "../../fixtures/tariff_price.json";

describe('Prepare product', () => {
    let categoryId;
    let warehouseId;
    let tariffId;

    before(() => {
        getConfigValue('category.default').then((id) => {
            categoryId = id;
        });

        getConfigValue('warehouse.default').then((id) => {
            warehouseId = id;
        });

        getConfigValue('tariff.list.default').then((id) => {
            tariffId = id;
        });
    })

     it('Create product', () => {
        cy.createProduct({
            ...product,
            warehouseId: warehouseId,
            categoryId: categoryId,
            tariffId: tariffId,
        }).then((product) => {
            setConfigValue('product.default', product.body);
            const productId = product.body.id;
            const defaultVariantId = product.body.defaultVariantId;
            const productCapabilityCron = 'Module%5CProduct%5CCapability%5CUniversalCapabilityCronJob';
            cy.assignCategoryToProduct(categoryId, {
                productId,
            });
            cy.reciveVariantInWarehouse(warehouseId, {
                productVariantId: defaultVariantId,
                quantity: 100,
            });
            cy.updateTariffPriceInProduct({
                ...tariffPrice,
                tariffId,
                price: 100,
                refId: productId,
            });
            cy.visibileProductInAlias({
                productIds: [productId],
            });
            cy.runCron(
                productCapabilityCron
            );
        });
    });
});
