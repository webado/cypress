import { setConfigValue } from '../../support/service/shopConfigurationManager';
import transportMethod from '../../fixtures/transport_method.json';

const countryId = Cypress.config('default').countryId;
const aliasId = Cypress.config('default').aliasId;

describe('Prepare transport method', () => {
    it('Create transport method', () => {
        cy.createTransportMethod({
            ...transportMethod,
            transportInCountry: [countryId],
            transportForAlias: [aliasId],
        }).then((transportMethod) => {
            setConfigValue('transport_method.default', transportMethod.body.id);
        });
    });
});
