import category from '../../fixtures/category.json';
import { setConfigValue } from '../../support/service/shopConfigurationManager';

describe('Prepare category', () => {
    it('Create category', () => {
        cy.createCategory(category).then((category) => {
            setConfigValue('category.default', category.body.id);
        });
    });
});
