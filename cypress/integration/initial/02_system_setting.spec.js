/// <reference types="cypress" />

describe('Set ss to specific value', () => {
    it('tariff ss', () => {
        const paramsTariffProvider = `?conditions[name][]=module.tariff.configuration.provider_symbol_to_migrate`;
        cy.findSystemSetting(paramsTariffProvider).then((response) => {
            cy.updateSystemSetting(response.body.items[0].id, 'tariff_read_provider');
        });
        const paramsProviderSymbol = `?conditions[name][]=module.tariff.configuration.provider_symbol`;
        cy.findSystemSetting(paramsProviderSymbol).then((response) => {
            cy.updateSystemSetting(response.body.items[0].id, 'tariff_read_provider');
        });
    });
});
