import { getConfigValue } from '../../support/service/shopConfigurationManager';
import weightRangeAndPaymentMethod from '../../fixtures/weight_range_and_payment_method.json';

let paymentMethodId;
let transportMethodId;
const currencyCode = Cypress.config('default').currencyCode;

before(() => {
    getConfigValue('payment_method.default').then((payment) => {
        paymentMethodId = payment;
    });

    getConfigValue('transport_method.default').then((transport) => {
        transportMethodId = transport;
    });
});

describe('Prepare weight range and finalization option', () => {
    it('Create weight range and finalization option', () => {
        const params = `?conditions['paymentMethodId'][]=${paymentMethodId}`;
        cy.findFinalizationOption(params).then(() => {
            cy.createWeightRangeAndFinalizationOption(transportMethodId, {
                ...weightRangeAndPaymentMethod,
                finalizationOptions: {
                    0: {
                        ...weightRangeAndPaymentMethod.finalizationOptions[0],
                        paymentMethodId: paymentMethodId,
                        currency: currencyCode,
                    },
                },
            });
        });
    });
});
